package com.farmaluz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.farmaluz.dao.IVentaDAO;
import com.farmaluz.model.Venta;
import com.farmaluz.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {
		// expresion lambda.
		venta.getDetalleVenta().forEach(det -> det.setVenta(venta));
		return dao.save(venta);
	}

	@Override
	public Venta modificar(Venta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Venta listarId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}
	
	
}
