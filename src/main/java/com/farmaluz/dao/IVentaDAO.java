package com.farmaluz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.farmaluz.model.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
