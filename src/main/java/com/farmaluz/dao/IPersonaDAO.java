package com.farmaluz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.farmaluz.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
